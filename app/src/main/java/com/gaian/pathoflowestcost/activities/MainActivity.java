package com.gaian.pathoflowestcost.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gaian.pathoflowestcost.Grid;
import com.gaian.pathoflowestcost.R;
import com.gaian.pathoflowestcost.adapter.TestCasetListAdapter;
import com.gaian.pathoflowestcost.utils.GridUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView testCasesRecyclerView;
    List<Grid> inputTestCases;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testCasesRecyclerView = (RecyclerView) findViewById(R.id.testCasesRecyclerView);
        testCasesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        setData();
    }

    private void setData(){
        inputTestCases = new ArrayList<>();
        inputTestCases.add(GridUtils.TEST_CASE_1);
        inputTestCases.add(GridUtils.TEST_CASE_2);
        inputTestCases.add(GridUtils.TEST_CASE_3);
        inputTestCases.add(GridUtils.TEST_CASE_4);
        inputTestCases.add(GridUtils.TEST_CASE_5);
       // inputTestCases.add(GridUtils.TEST_CASE_7);
        inputTestCases.add(GridUtils.TEST_CASE_8);
        inputTestCases.add(GridUtils.TEST_CASE_9);
        inputTestCases.add(GridUtils.TEST_CASE_10);
        inputTestCases.add(GridUtils.TEST_CASE_11);
        inputTestCases.add(GridUtils.TEST_CASE_12);
      //  inputTestCases.add(GridUtils.TEST_CASE_13); //

        TestCasetListAdapter testCastListAdapter = new TestCasetListAdapter(MainActivity.this,inputTestCases);
        testCasesRecyclerView.setAdapter(testCastListAdapter);

    }
}
