package com.gaian.pathoflowestcost.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gaian.pathoflowestcost.Grid;
import com.gaian.pathoflowestcost.GridVisitor;
import com.gaian.pathoflowestcost.PathState;
import com.gaian.pathoflowestcost.R;

import java.util.List;

public class TestCasetListAdapter extends RecyclerView.Adapter<TestCasetListAdapter.ItemViewHolder> {

    Context context;
    List<Grid> inputList;

    public TestCasetListAdapter(Context context, List<Grid> inputList) {
        this.context = context;
        this.inputList = inputList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_row, parent, false);

        return new ItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        Grid validGrid = inputList.get(position);
        GridVisitor visitor = new GridVisitor(validGrid);
        PathState bestPath = visitor.getBestPathForGrid();

        if (bestPath.isSuccessful()) {
            holder.results_success.setText("Yes");

        } else {
            holder.results_success.setText("No");
        }

        holder.results_total_cost.setText(Integer.toString(bestPath.getTotalCost()));
        holder.results_path_taken.setText(formatPath(bestPath));
        holder.grid_contents.setText(validGrid.asDelimitedString("\t"));

    }

    @Override
    public int getItemCount() {
        return inputList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView results_success;
        TextView results_total_cost;
        TextView results_path_taken;
        TextView grid_contents;

        public ItemViewHolder(View itemView) {
            super(itemView);

            results_success = (TextView)itemView.findViewById(R.id.results_success);
            results_total_cost = (TextView)itemView.findViewById(R.id.results_total_cost);
            results_path_taken = (TextView)itemView.findViewById(R.id.results_path_taken);
            grid_contents = (TextView)itemView.findViewById(R.id.grid_contents);
        }
    }



    private String formatPath(PathState path) {
        StringBuilder builder = new StringBuilder();
        List<Integer> rows = path.getRowsTraversed();

        for (int i = 0; i < rows.size(); i++) {
            builder.append(rows.get(i));
            if (i < rows.size() - 1) {
                builder.append("\t");
            }
        }

        return builder.toString();
    }
}
